//
//  FirstViewController.h
//  ReminderApp
//
//  Created by finskii on 09/04/15.
//  Copyright (c) 2015 finskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
@interface FirstViewController : UIViewController

@property (strong, nonatomic) EKEventStore *eventStore;
@property (strong, nonatomic) EKCalendar *calendar;
@property (weak, nonatomic) IBOutlet UITextField *reminderText;
@property (weak, nonatomic) IBOutlet UIDatePicker *myDatePicker;
- (IBAction)setReminder:(id)sender;



@end

