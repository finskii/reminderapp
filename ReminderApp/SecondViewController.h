//
//  SecondViewController.h
//  ReminderApp
//
//  Created by finskii on 09/04/15.
//  Copyright (c) 2015 finskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import <CoreLocation/CoreLocation.h>
#define LOC_COORDINATE = +51.50998000;-0.13370000;
#define LOC_IDENTIFIER = @"kokoko"


@interface SecondViewController : UIViewController
<CLLocationManagerDelegate>

@property (strong, nonatomic) EKEventStore *eventStore;
@property (strong, nonatomic) CLLocationManager *manager;
@property (strong, nonatomic) EKCalendar *calendar;


@property (strong, nonatomic) IBOutlet UISwitch *switchEnabled;
@property (nonatomic, strong) NSMutableArray *locations;

@property (weak, nonatomic) IBOutlet UILabel *notificationCoutLabel;

@property (strong, nonatomic) IBOutlet UITextField *locationText;

- (IBAction)setLocationReminder:(id)sender;
- (void) timerHandler:(NSTimer*)timer;
@property(nonatomic, copy) NSArray *scheduledLocalNotifications;
@end

