//
//  SecondViewController.m
//  ReminderApp
//
//  Created by finskii on 09/04/15.
//  Copyright (c) 2015 finskii. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (nonatomic, strong) NSTimer *repeatingTimer;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.locationText becomeFirstResponder];
    [self promptForPermission];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.manager = [[CLLocationManager alloc]init];
    self.manager.distanceFilter = kCLDistanceFilterNone;
    self.manager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locations = [[NSMutableArray alloc] init];
    self.manager.delegate = self;
    [self.switchEnabled setOn:NO];
    
        int i = [UIApplication sharedApplication].scheduledLocalNotifications.count;
    NSString *ii = [@(i) stringValue];
    self.notificationCoutLabel.text = ii;
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    if ([self.manager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [self.manager requestAlwaysAuthorization];
    [UIApplication sharedApplication].scheduledLocalNotifications;
    UILocalNotification* obj = [[[UIApplication sharedApplication] scheduledLocalNotifications] firstObject];
}

- (void)startShowingNotifications:(NSString*)msg {
//    float latitude = 58.01503681;
//    float longitude = 56.23163393;
    float latitude = 58.01480670970106;
    float longitude = 56.232556940661944;
    
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude=longitude;
    
    UILocalNotification *locNotification = [[UILocalNotification alloc] init];
    locNotification.alertBody = msg;
    locNotification.regionTriggersOnce = NO;
    locNotification.region = [[CLCircularRegion alloc]
                              initWithCenter:center
                              radius:1000
                              identifier:@"kokoko identifier"];
    locNotification.region.notifyOnEntry = YES;
    locNotification.region.notifyOnExit = YES;

    
    [[UIApplication sharedApplication] scheduleLocalNotification:locNotification];

    
}
- (void)showMoreNotifications:(NSString*)msg {
    //    float latitude = 58.01503681;
    //    float longitude = 56.23163393;
    float latitude = 58.01480670970106;
    float longitude = 56.232556940661944;
    
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude=longitude;
    
    UILocalNotification *locNotification = [[UILocalNotification alloc] init];
    locNotification.alertBody = msg;
    locNotification.regionTriggersOnce = NO;
    locNotification.region = [[CLCircularRegion alloc]
                              initWithCenter:center
                              radius:1000
                              identifier:@"kokoko new"];
    locNotification.region.notifyOnEntry = YES;
    locNotification.region.notifyOnExit = YES;
    
    
    [[UIApplication sharedApplication] scheduleLocalNotification:locNotification];
    
    
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification: (UILocalNotification *)notification {
    
    CLRegion *region = notification.region;

    if (region) {
        NSLog(@"KOKOKO");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    
//    UILocalNotification *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
//    if (localNotif) {
//        localNotif.alertBody = @"You have arrived!";
//    }
//}
//- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
//{
//    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
//        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
//    }
//    // Override point for customization after application launch.
//    return YES;
//}

- (void) promptForPermission
{
        UIUserNotificationType types;
        UIUserNotificationSettings *settings;
        
        types = UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound;
        
        settings = [UIUserNotificationSettings settingsForTypes:types
                                                     categories:nil];
        
            [[UIApplication sharedApplication]
             registerUserNotificationSettings:settings];
    }



-(void)setLocationReminder:(id)sender
{
    if (_eventStore == nil)
    {
        _eventStore = [[EKEventStore alloc]init];
        
        [_eventStore requestAccessToEntityType:EKEntityTypeReminder completion:^(BOOL granted, NSError *error) {
            
            if (!granted)
                NSLog(@"Access to store not granted");
        }];
    }
    
    if (_eventStore)
    {
        [self.manager startUpdatingLocation];
    }
}

- (EKCalendar *)calendar {
    if (!_calendar) {
        
        // 1
        NSArray *calendars = [self.eventStore calendarsForEntityType:EKEntityTypeReminder];
        
        // 2
        NSString *calendarTitle = @"UDo!";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title matches %@", calendarTitle];
        NSArray *filtered = [calendars filteredArrayUsingPredicate:predicate];
        
        if ([filtered count]) {
            _calendar = [filtered firstObject];
        } else {
            
            // 3
            _calendar = [EKCalendar calendarForEntityType:EKEntityTypeReminder eventStore:self.eventStore];
            _calendar.title = @"FuckLoc!";
            _calendar.source = self.eventStore.defaultCalendarForNewReminders.source;
            
            // 4
            NSError *calendarErr = nil;
            BOOL calendarSuccess = [self.eventStore saveCalendar:_calendar commit:YES error:&calendarErr];
            if (!calendarSuccess) {
                // Handle error
            }
        }
    }
    return _calendar;
}

- (IBAction)enabledStateChanged:(id)sender
{
//    [self startRepeatingTimer];
    if (self.switchEnabled.on)
    {
        //        [self.manager startUpdatingLocation];
        [self startShowingNotifications:@"kokoko"];
        [self showMoreNotifications:@"azaza"];
    }
    else
    {
        [self.manager stopUpdatingLocation];
    }
}

- (void)enabledState
{
    if (self.switchEnabled.on)
    {
        [self.switchEnabled setOn:NO];
    }
    if (!self.switchEnabled.on)
    {
        [self.switchEnabled setOn:YES];
    }
}

- (void)startRepeatingTimer
{
    self.repeatingTimer = [NSTimer scheduledTimerWithTimeInterval:10.0
                                                           target:self
                                                         selector:@selector(timerHandler:)
                                                         userInfo:nil
                                                          repeats:YES];
}

- (void) timerHandler:(NSTimer*)timer
{
    [self.manager stopUpdatingLocation];
    [self.manager startUpdatingLocation];

}


#pragma mark -
#pragma mark CLLocationManagerDelegate


- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
//    [self.manager stopUpdatingLocation];
    
    EKReminder *reminder = [EKReminder
                            reminderWithEventStore:_eventStore];

    NSDate* currentDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* nowTimeZone = [NSTimeZone systemTimeZone];
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:currentDate];
    NSInteger nowGMTOffset = [nowTimeZone secondsFromGMTForDate:currentDate];
    
    NSTimeInterval interval = nowGMTOffset - currentGMTOffset;
    NSDate* nowDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:currentDate];
    
//    reminder.title = currentDate;
//    reminder.calendar = self.calendar;
    NSLog(@"Fuck loc setted date : %@", currentDate);
    NSLog(@"Fuck loc setted date : %@", newLocation);


    EKAlarm *alarm = [EKAlarm alarmWithAbsoluteDate:nowDate];
    
    [reminder addAlarm:alarm];
    
    NSError *error = nil;
    
    [_eventStore saveReminder:reminder commit:YES error:&error];
    
    if (error)
        NSLog(@"Failed to set reminder: %@", error);
//    [self.manager startUpdatingLocation];
}



- (IBAction)hideKeyboard:(id)sender {
    [self.locationText resignFirstResponder];
    [self.locationText endEditing:YES];
}









@end
